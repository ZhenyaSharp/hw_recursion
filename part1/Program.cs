﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace part1
{
    class Program
    {//factorial
        static int GetFact(int n)
        {
            if (n > 1)
            {
                return n * GetFact(n - 1);
            }
            else
            {
                return n;
            }
        }
        //VozvStepen
        static int MathPow(int a, int n)
        {
            if (n > 0)
            {
                return a * MathPow(a, n - 1);
            }
            else
            {
                return 1;
            }
        }
            static void Main(string[] args)
        {
            int FactN = GetFact(5);
            Console.WriteLine("Factorial= "+FactN);

            int Stepen = MathPow(2, 5);
            Console.WriteLine($"Stepen= {Stepen}");
            Console.ReadKey();

            /*task 10_51
             a)54321
             b)12345
             B)54321012345

           */
        }
    }
}
