﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_48
{
    class Program
    {
        static int SearchMaxNum(int[] mas, ref int i, ref int max, ref int index)
        {
            if (i < mas.Length)
            {
                if (mas[i] > max)
                {
                    max = mas[i];
                    index = i;
                }
                i++;
                SearchMaxNum(mas, ref i, ref max, ref index);
            }
            return index;
        }

        static void Main(string[] args)
        {
            int[] mas = new int[] { 3, 5, 2, 3, 7, 11, 1, 8 };
            int i = 0;
            int max = 0;
            int index = 0;
            int resultIndex = 0;

            resultIndex= SearchMaxNum(mas, ref i, ref max, ref index);
            Console.WriteLine("index max diggit= " + resultIndex);

            Console.ReadKey();
        }
    }
}
