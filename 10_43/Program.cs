﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_43
{
    class Program
    {
        static void SumDiggit(int n, ref int sum)
        {
            if (n != 0)
            {
                sum += (n % 10);
                SumDiggit(n /= 10, ref sum);
            }
        }
            
        static void Main(string[] args)
        {
            Console.WriteLine("Input number");
            int n = int.Parse(Console.ReadLine());

            int sum = 0;

            SumDiggit(n, ref sum);
            Console.WriteLine("Sum diggit= " + sum);
            Console.ReadKey();
        }
    }
}
